console.log('start skriptu');

const pocet = prompt('Zadej počet dílků', '1');

try {
  const pocetAsNumber = parseFloat(pocet);

  if (Number.isNaN(pocetAsNumber)) {
    throw 'Error: nebylo zadáno číslo';
  }
  const vzorec = `10 * ${pocet}`;
  const vysledek = eval(vzorec);

  console.log(vysledek);
} catch (error) {
  console.error(error);
  alert ('Nebylo zadáno číslo');
}

console.log('konec scriptu');
