const BR = '<br>';
const ADULT_AGE = 18;
const SENIOR_AGE = 64;

document.write('Vítejte v mé hospodě!');
document.write(BR);

const age = getIntegerFromUser(
  'Zadej svůj věk (celé číslo)',
  '1',
);


const isAdult = age >= ADULT_AGE;

let output = 'Vítejte, hned přinesu ohříváček.';

if (!isAdult) {
  output = 'Sorry, mladistvým nenalejvám!';
} else if (age < SENIOR_AGE) {
  output = 'Nazdar a drž úhel.';
}

document.write(output);
document.write(BR);

if (isAdult) {
  const MAX_BEERS_PER_ORDER = 18;

  const beerCount = getIntegerFromUser(
    `Kolik chceš piv? Maximum je ${MAX_BEERS_PER_ORDER}.`,
    '1',
    0,
    MAX_BEERS_PER_ORDER,
  );

  let substantivum;
  switch (beerCount) {
    case 1:
      substantivum = 'pivo';
      break;
    case 2:
    case 3:
    case 4:
      substantivum = 'piva';
      break;
    default:
      substantivum = 'piv';
  }

  document.write(
    `Děkuji za objednávku, za chvíli donesu ${beerCount} ${substantivum}.`,
  );
  document.write(BR);
  /*
  for (let beerNr = 1; beerNr <= beerCount; beerNr++) {
    document.write(`Tady máte ${beerNr}. pivo.`);
    document.write(BR);
  }
  */

  for (let beerInHand = beerCount; beerInHand > 0; beerInHand--) {
    const beerInTable = beerCount - beerInHand;
    document.write(`Tady máte ${beerInTable + 1}. pivo.`);
    document.write(BR);
  }
}

/*
const MY_URL = 'https://hotel.test.cz';
const MAX_HOT_ID = 10;
const MAX_CURRENCY = 3;
const LANGUAGE = 'cs';
const SHOW_TABS = 1;

for (let hotId = 1; hotId <= MAX_HOT_ID; hotId++) {
  for (let currency = 1; currency <= MAX_CURRENCY; currency++) {
    const params = new URLSearchParams({
      hotId,
      currency,
      lang: LANGUAGE,
      showTabs: SHOW_TABS,
    });

    const finalUrl = `${MY_URL}?${params.toString()}`;
    document.write(finalUrl);
    document.write(BR);
  }
}
*/
