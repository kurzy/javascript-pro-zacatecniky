console.log('funkce.js started');

const randomNumber = Math.ceil(Math.random() * 6);
console.log('🤘 ~ file: funkce.js:4 ~ randomNumber:', randomNumber);

const apiUrl = 'https://jsonplaceholder.typicode.com/users/{userId}';

const userId = 3;
const urlWithUserId = apiUrl.replace('{userId}', userId);

console.log(urlWithUserId);

function sayHello1 () {
  console.log('Nazdar, programátore!');
}

sayHello1();

function sayHello2(name) {
  console.log(`Nazdar, ${name}!`);
}

sayHello2('Karle');
sayHello2('Supermane');
sayHello2('frajere Batmane');
sayHello2('programátore');
sayHello2();

function sayHello3(name = 'ó, veliký, mocný a slavný programátore') {
  console.log(`Nazdar, ${name}!`);
}

sayHello3('bobane');
sayHello3();

console.log('součet 4 + 2 =', sum(4, 2));

function sum (x, y) {
  const result = x + y;

  return result;
}

const sum2 = function(x, y) {
  const result = x + y;

  return result;
};

console.log('součet 5 + 3 =', sum2(5, 3));

const sum3 = (x, y) => {
  const result = x + y;

  return result;
};

console.log('součet 6 + 4 =', sum3(6, 4));

const sum4 = (x, y) => {
  return x + y;
};

console.log('součet 7 + 5 =', sum4(7, 5));

const sum5 = (x, y) => x + y;

console.log('součet 8 + 6 =', sum5(8, 6));

const getBoxVolume = () => {
  const sideA = getIntegerFromUser('Zadej stranu A');
  const sideB = getIntegerFromUser('Zadej stranu B');
  const sideC = getIntegerFromUser('Zadej stranu C');

  return sideA * sideB * sideC;
}

console.log(getBoxVolume());
