const dom = {
  button: document.querySelector('#clickMe'),
  images: document.querySelectorAll('#testImages img'),
  navAnchors: document.querySelectorAll('nav a'),
  output: document.querySelector('#output'),
};

const log = (message = '') => {
  const span = document.createElement('span');
  span.textContent = message;

  dom.output.appendChild(span);
  dom.output.scrollTop = dom.output.scrollHeight;
};

log('Skript spuštěn!');

const navAnchorClickHandler = (event) => {
  if (event.ctrlKey) {
    return;
  }

  event.preventDefault();

  const randomDec = Math.round(Math.random() * 255) ** 3;
  const randomColor = `#${randomDec.toString(16)}`;
  event.target.style.color = randomColor;
}

const modifyNavAnchors = () => {
  log ('Modifikuji odkazy');

  for (let i = 0; i < dom.navAnchors.length; i++) {
    const anchor = dom.navAnchors[i];
    anchor.target = '_blank';
    anchor.addEventListener('click', navAnchorClickHandler);
  }
};

modifyNavAnchors();

const imageAnchors = [
  './images/A3a.gif',
  './images/B25a.gif',
  './images/B27a.gif',
  './images/A3b.gif',
  './images/B25b.gif',
  './images/B27b.gif',
];

const toggleImageSrc = (image, index) => {
  const url = imageAnchors[index];
  log(`Přepínám obrázek na ${url}`);
  image.src = url;
};

const modifyImages = () => {
  log('Připravuju obrázky');

  const imageCount = dom.images.length;
  for (let i = 0; i < imageCount; i++) {
    const image = dom.images[i];

    image.addEventListener('mouseover', () => toggleImageSrc(image, imageCount + i));
    image.addEventListener('mouseout', () => toggleImageSrc(image, i));
  }
};

modifyImages();

const classNames = [
  'red', 'green', 'blue', 'yellow', 'magenta', 'azure',
];

const buttonClickHandler = (event) => {
  const x = event.offsetX;
  log(`Kliknul jsem do vzdálenosti ${x}`);

  const buttonWidth = dom.button.clientWidth;
  const barWidth = buttonWidth / (classNames.length + 1);
  const barIndex = Math.floor(x / barWidth);

  log (`kliknul jsem do oblasti ${barIndex}`);

  for (let i = 0; i < classNames.length; i++) {
    dom.button.classList.remove(classNames[i]);
  }

  const cName = classNames[barIndex];
  if (!!cName) {
    dom.button.classList.add(cName);
  }
}

dom.button.addEventListener('click', buttonClickHandler);
