const inRange = (number, min = 0, max) => {
  if(typeof max !== 'undefined' && max <= min) {
    throw 'Max is not higher then min!';
  }

  const higherOrEqThenMin = min <= number;

  const lowerOrEqThenMax = typeof max !== 'undefined'
    ? max >= number
    : true;

  return higherOrEqThenMin && lowerOrEqThenMax;
};

const getIntegerFromUser = (
  message,
  defaultValue,
  min,
  max
) => {
  let numberValue;
  let valueIsOk = false;

  do {
    const stringValue = prompt(message, defaultValue);
    numberValue = parseInt(stringValue);
    const isSame = numberValue.toString() === stringValue;
    const isInRange = inRange(numberValue, min, max);

    valueIsOk = isSame && isInRange;
  } while(!valueIsOk);

  return numberValue;
};
