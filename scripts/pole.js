const left = ['Vilda', 'Bára'];
const right = new Array('Maria', 'Ondra', 'Alisa', 'Honza');

console.log(left);
console.log(right);

/*
let x = 5;
let y = x;
console.log(x, y);
x++;
console.log(x, y);

const leftCopy = left;
left.push('Frodo');
console.log(left, leftCopy);

const a1 = [1];
const a2 = [1];
console.log(a1 === a2);
*/

const leftCopy = [...left];
left.push('Peťa');

console.log(left, leftCopy);

const allMembers = [...left, ...right];
console.log(allMembers);

const cookieIndex = Math.floor(Math.random() * allMembers.length);
console.log(`Piškotek dostane ${allMembers[cookieIndex]}.`);

const includesI = (arrayItem) => arrayItem
  .toLowerCase()
  .includes('i');

console.log('includesI Ivan', includesI('Ivan'));
console.log('includesI Magda', includesI('Magda'));

const membersWithI = allMembers.filter(includesI);
console.log('membersWithI', membersWithI);

const notIncludesI = (arrayItem) => !arrayItem
  .toLowerCase()
  .includes('i');

console.log('notIncludesI Ivan', notIncludesI('Ivan'));
console.log('notIncludesI Magda', notIncludesI('Magda'));

const membersWithoutI = allMembers.filter(notIncludesI);
console.log('membersWithoutI', membersWithoutI);

const addRandomStars = (arrayItem) => {
  const randomStarsCount = Math.ceil(Math.random() * 5);
  const star = '⭐';
  const stars = star.repeat(randomStarsCount);

  return `${arrayItem} ${stars}`;
};

const membersWithStars = allMembers.map(addRandomStars);
console.log('membersWithStars', membersWithStars);

const getLength = (item) => item.split('').length;

const sumLength = (acc, item) => acc + getLength(item);

const membersCharLength = allMembers.reduce(sumLength, 0);
console.log('membersCharLength', membersCharLength);

const createFibonacci = (length) => {
  if (typeof length !== 'number' || length < 2) {
    throw 'Počet prvků posloupnosti musí být alespoň 2!';
  }

  const base = [BigInt(0), BigInt(1)];

  for (let i = 2; i < length; i++) {
    const newItem = base[i - 1] + base[i - 2];
    base.push(newItem);
  }

  return base;
}

// createFibonacci(1);
const fib10 = createFibonacci(10);
const fib100 = createFibonacci(100);

console.log('fib10', fib10);
console.log('fib100', fib100);

const isEven = (number) => !(number % BigInt(2));
console.log('is even 42', isEven(BigInt(42)));
console.log('is even 11', isEven(BigInt(11)));

const evenFib100 = fib100.filter(isEven);
console.log('evenFib100', evenFib100);
console.log('fib(100)', fib100[99], Number.isSafeInteger(fib100[99]));
